#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLineEdit>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:
    void add(){
        delete this->centralWidget();
        /*
        QHBoxLayout *layout = new QHBoxLayout;
        layout->addWidget(title);

        QWidget *widget = new QWidget;

        this->setCentralWidget(widget);
        widget->setLayout(layout);
        */
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void build(){
        QWidget *widget = new QWidget;

        QPushButton *add = new QPushButton();
        add->setText("Ajouter une recette");

        QObject::connect(add, SIGNAL(clicked()), this , SLOT(add()));

        QPushButton *search = new QPushButton();
        search->setText("Rechercher une recette");

        QHBoxLayout *layout = new QHBoxLayout;
        layout->addWidget(add);
        layout->addWidget(search);
        this->setCentralWidget(widget);
        widget->setLayout(layout);

        this->setWindowTitle("Mon livre de recettes");
    };

private:
    Ui::MainWindow *ui;
};

